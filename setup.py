from setuptools import setup

setup(
   name="my_algorithms",
   version='1.0',
   py_modules=[
      "graphs",
      "geometry_2i",
   ],
)
