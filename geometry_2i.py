from dataclasses import dataclass


@dataclass(frozen=True)
class Point2i:
    """
    Simple 2D point with integer coordinates.
    """
    x: int
    y: int

    def __add__(self, dir: "Vector2i") -> "Point2i":
        """
        Applying a vector to a point gives a new point.
        """
        return Point2i(self.x + dir.x, self.y + dir.y)

    def get_distance(self, other: "Point2i") -> int:
        """
        Calculate the manhatten distance of two points.
        """
        return abs(self.x - other.x) + abs(self.y - other.y)


@dataclass(frozen=True)
class Vector2i:
    """
    Contrary to a point, a vector is not about a specific location but about a change in location.
    A vector can be applied to a point to move it by the amounts given.
    """
    x: int
    y: int

    def __mul__(self, f: int) -> "Vector2i":
        """
        Multiplying by a scalar factor lengthens the vector, but does not change direction.
        """
        return Vector2i(self.x * f, self.y * f)

    def rotate_clockwise(self) -> "Vector2i":
        """
        Rotate by 90° clockwise.
        """
        return Vector2i(-self.y, self.x)

    def rotate_counter_clockwise(self) -> "Vector2i":
        """
        Rotate by 90° counter-clockwise.
        """
        return Vector2i(self.y, -self.x)


@dataclass(frozen=True)
class Rectangle:
    """
    Simple 2D rectangle represented by its top-left and bottom-right corners.
    """
    left_top: Point2i
    right_bottom: Point2i

    def get_area(self) -> int:
        width = self.right_bottom.x - self.left_top.x + 1
        height = self.right_bottom.y - self.left_top.y + 1
        return width * height
