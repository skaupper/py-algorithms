import heapq
from dataclasses import dataclass
from typing import TypeVar, Generic, Hashable, Optional, List, Protocol, Dict, Tuple, Set

T = TypeVar("T", bound=Hashable)


#
# General graph classes
#
@dataclass(frozen=True, eq=True)
class Edge(Generic[T]):
    dest: T
    weight: float = 1.0


# -----------------------------------------------------------------------------
#
# Pathfinding
#
class PathFinderProtocol(Protocol[T]):
    """
    A class with those methods has to be implemented when you want to use `get_shortest_path`.
    They represent all the customization one needs to do.
    """

    def get_neighbours(self, point: T) -> List[Edge[T]]:
        """
        Return all outgoing edges from the given point.
        """
        ...

    def get_distance_to_target(self, point: T) -> float:
        """
        Get the minimum distance from the given point to the target point.
        """
        return 1

    def is_target(self, point: T) -> bool:
        """
        Check if the given point is the target point.
        """
        return False


# Intermediate node needed for the A* implementation
@dataclass(frozen=True)
class _IntermediateNodeAStar(Generic[T]):
    distance_to_start: float
    min_distance_to_dest: float
    point: T

    def __lt__(self, other: "_IntermediateNodeAStar[T]") -> bool:
        return (self.distance_to_start  + self.min_distance_to_dest ) < \
               (other.distance_to_start + other.min_distance_to_dest)


# Intermediate node needed for the Dijkstra implementation
@dataclass(frozen=True)
class _IntermediateNodeDijkstra(Generic[T]):
    distance_to_start: float
    point: T

    def __lt__(self, other: "_IntermediateNodeDijkstra[T]") -> bool:
        return self.distance_to_start < other.distance_to_start


def _backtrack_path(
    point: T,
    visited: Dict[T, Tuple[float, Optional[T]]],
) -> List[T]:
    path: List[T] = [point]
    _, prev = visited[point]

    while prev is not None:
        path.append(prev)
        _, prev = visited[prev]

    return list(reversed(path))


# Implementation of A*
def get_shortest_path(
    start: T,
    methods: PathFinderProtocol[T],
) -> Optional[List[T]]:
    visited: Dict[T, Tuple[float, Optional[T]]] = {start: (0.0, None)}
    candidates = [_IntermediateNodeAStar(0, methods.get_distance_to_target(start), start)]

    while len(candidates) != 0:
        node = heapq.heappop(candidates)

        if methods.is_target(node.point):
            return _backtrack_path(node.point, visited)

        for neighbour in methods.get_neighbours(node.point):
            if neighbour.dest in visited:
                continue

            # Add all neighbours to the heap
            new_node = _IntermediateNodeAStar(
                node.distance_to_start + neighbour.weight,
                methods.get_distance_to_target(neighbour.dest),
                neighbour.dest,
            )
            heapq.heappush(candidates, new_node)

            # Update the minimal distance to any visited point
            entry = visited.get(new_node.point, None)
            if entry is None:
                visited[new_node.point] = (
                    new_node.distance_to_start,
                    node.point,
                )
            else:
                visited[new_node.point] = (
                    min(new_node.distance_to_start, entry[0]),
                    node.point,
                )

    return None


# Implementation of Dijkstra
def get_all_shortest_paths(
    start: T,
    methods: PathFinderProtocol[T],
) -> Dict[T, Tuple[float, List[T]]]:
    visited: Dict[T, Tuple[float, List[T]]] = {start: (0.0, [])}
    candidates = [_IntermediateNodeDijkstra(0, start)]

    while len(candidates) != 0:
        node = heapq.heappop(candidates)

        for neighbour in methods.get_neighbours(node.point):
            dist = node.distance_to_start + neighbour.weight

            if neighbour.dest in visited:
                if visited[neighbour.dest][0] == dist:
                    visited[neighbour.dest][1].append(node.point)
                continue

            # Add all neighbours to the heap
            new_node = _IntermediateNodeDijkstra(
                dist,
                neighbour.dest,
            )
            heapq.heappush(candidates, new_node)

            # Update the minimal distance to any visited point
            entry = visited.get(new_node.point, None)
            if entry is None:
                visited[new_node.point] = (
                    new_node.distance_to_start,
                    [node.point],
                )
            else:
                visited[new_node.point] = (
                    min(new_node.distance_to_start, entry[0]),
                    [node.point],
                )

    return visited


# -----------------------------------------------------------------------------
