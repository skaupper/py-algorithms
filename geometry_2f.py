from dataclasses import dataclass
from typing import Tuple, overload, Union
import enum
import math

EPSILON = 1e-10


class AngleUnit(enum.Enum):
    RADIANS = enum.auto()
    DEGREES = enum.auto()


@dataclass(frozen=True)
class Angle:
    _unit: AngleUnit
    _value: float

    @classmethod
    def from_degrees(cls, value: float) -> "Angle":
        return cls(_unit=AngleUnit.DEGREES, _value=value)

    @classmethod
    def from_radians(cls, value: float) -> "Angle":
        return cls(_unit=AngleUnit.RADIANS, _value=value)

    def to_radians(self) -> "Angle":
        if self._unit == AngleUnit.RADIANS:
            return self

        return Angle(
            _unit=AngleUnit.RADIANS,
            _value=self._value / 180 * math.pi,
        )

    def to_degrees(self) -> "Angle":
        if self._unit == AngleUnit.DEGREES:
            return self

        return Angle(
            _unit=AngleUnit.DEGREES,
            _value=self._value / math.pi * 180,
        )

    def get_value(self, unit: AngleUnit) -> float:
        if unit == self._unit:
            return self._value

        if self._unit == AngleUnit.RADIANS and unit == AngleUnit.DEGREES:
            return self.to_degrees()._value
        elif self._unit == AngleUnit.DEGREES and unit == AngleUnit.RADIANS:
            return self.to_radians()._value

        assert False

    def __add__(self, other: "Angle") -> "Angle":
        return Angle(
            _unit=AngleUnit.RADIANS,
            _value=other.to_radians()._value + self.to_radians()._value,
        )

    def __sub__(self, other: "Angle") -> "Angle":
        return Angle(
            _unit=AngleUnit.RADIANS,
            _value=other.to_radians()._value - self.to_radians()._value,
        )

    def __neg__(self) -> "Angle":
        return Angle(_unit=AngleUnit.RADIANS, _value=-self._value)

    def __pos__(self) -> "Angle":
        return self


@dataclass(frozen=True)
class Point2f:
    x: float
    y: float

    def __add__(self, vector: "Vector2f") -> "Point2f":
        return Point2f(self.x + vector.x, self.y + vector.y)

    def __sub__(self, other: "Vector2f") -> "Point2f":
        return Point2f(self.x - other.x, self.y - other.y)

    def get_direction(self, other: "Point2f") -> "Vector2f":
        return Vector2f(other.x - self.x, other.y - self.y)


@dataclass(frozen=True)
class Vector2f:
    x: float
    y: float

    def __add__(self, vector: "Vector2f") -> "Vector2f":
        return Vector2f(self.x + vector.x, self.y + vector.y)

    def __mul__(self, scalar: float) -> "Vector2f":
        return Vector2f(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar: float) -> "Vector2f":
        return Vector2f(self.x / scalar, self.y / scalar)

    def __floordiv__(self, scalar: float) -> "Vector2f":
        return Vector2f(self.x // scalar, self.y // scalar)

    def rotate(self, angle: Angle) -> "Vector2f":
        angle_raw = angle.get_value(AngleUnit.RADIANS)
        return Vector2f(
            self.x * math.cos(angle_raw) - self.y * math.sin(angle_raw),
            self.x * math.sin(angle_raw) + self.y * math.cos(angle_raw),
        )

    def normalize(self) -> "Vector2f":
        return self / self.abs()

    def abs(self) -> float:
        return math.sqrt(self.x**2 + self.y**2)

    def get_angle(self) -> float:
        return math.atan2(self.y, self.x)


# ######################################################################################################################


def move_on_circle(
    pos: Point2f,
    direction: Vector2f,
    distance_on_circle: float,
    circle_center: Point2f,
) -> Tuple[Point2f, Vector2f]:
    # Create a vector from the start point to the middle of the circle
    pos_to_center = pos.get_direction(circle_center)
    radius = pos_to_center.abs()
    assert radius > 0

    # The two vectors given by (pos, direction) and (pos, circle_center) must be perpendicular to one another.
    # Otherwise the calculation of the new direction below is not working correctly!
    assert abs(
        (pos_to_center.get_angle() - direction.get_angle()) -  #
        math.pi / 2,  #
    ) < EPSILON

    # The angle travelled on the circle (in radians) is proportional to the radius of the circle
    angle_of_end_point = Angle.from_radians(distance_on_circle / radius)


    # Given the angle of the end point on the circle, one can calculate the coordinates of the new position
    new_pos = circle_center - \
        pos_to_center * math.cos(angle_of_end_point.get_value(AngleUnit.RADIANS)) * radius + \
            direction * math.sin(angle_of_end_point.get_value(AngleUnit.RADIANS)) * radius
    # The new direction is calculated using the calculated angle
    new_dir = direction.rotate(-angle_of_end_point)

    return new_pos, new_dir
